<?php
// $id:

/**
 * @file
 * Provides an input filter to add nofollow to rel attribute in links
 * while converting links to URLs
 *
 * based on nofollow module by: Hideki Ito (PIXTURE STUDIO)
 */

/**
 * Implementation of hook_filter().
 *
 * Provides a new, improved URL filter that adds nofollow rel attr to link tag
 */
function nofollowurlfilter_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return array(0 => t('nofollow url filter'));

    case 'description':
      switch ($delta) {
        case 0:
          return t('convert URLs to links with nofollow attribute, no trimming!');
        default:
          return;
      }

    case 'process':
      switch ($delta) {
        case 0:
          return _filter_nofollowurl($text);
        default:
          return $text;
      }

    default:
      return $text;
  }
}


/**
 * Add rel="nofollow" attribute to anchor tags during URL filter
 */
function _filter_nofollowurl($text) {
  $text = ' '. $text .' ';

  // Match absolute URLs.
  $text = preg_replace_callback("`(<p>|<li>|<br\s*/?>|[ \n\r\t\(])((http://|https://|ftp://|mailto:|smb://|afp://|file://|gopher://|news://|ssl://|sslv2://|sslv3://|tls://|tcp://|udp://)([a-zA-Z0-9@:%_+*~#?&=.,/;-]*[a-zA-Z0-9@:%_+*~#&=/;-]))([.,?!]*?)(?=(</p>|</li>|<br\s*/?>|[ \n\r\t\)]))`i", '_filter_nofollowurl_parse_full_links', $text);

  // Match e-mail addresses.
  $text = preg_replace("`(<p>|<li>|<br\s*/?>|[ \n\r\t\(])([A-Za-z0-9._-]+@[A-Za-z0-9._+-]+\.[A-Za-z]{2,4})([.,?!]*?)(?=(</p>|</li>|<br\s*/?>|[ \n\r\t\)]))`i", '\1<a href="mailto:\2">\2</a>\3', $text);

  // Match www domains/addresses.
  $text = preg_replace_callback("`(<p>|<li>|[ \n\r\t\(])(www\.[a-zA-Z0-9@:%_+*~#?&=.,/;-]*[a-zA-Z0-9@:%_+~#\&=/;-])([.,?!]*?)(?=(</p>|</li>|<br\s*/?>|[ \n\r\t\)]))`i", '_filter_nofollowurl_parse_partial_links', $text);
  $text = substr($text, 1, -1);

  return $text;

}

/**
 * Make links out of absolute URLs.
 */
function _filter_nofollowurl_parse_full_links($match) {
  $match[2] = decode_entities($match[2]);
  $caption = check_plain($match[2]);
  $match[2] = check_url($match[2]);
  return $match[1] .'<a rel="nofollow" href="'. $match[2] .'" title="'. $match[2] .'">'. $caption .'</a>'. $match[5];
}

/**
 * Make links out of domain names starting with "www."
 */
function _filter_nofollowurl_parse_partial_links($match) {
  $match[2] = decode_entities($match[2]);
  $caption = check_plain($match[2]);
  $match[2] = check_plain($match[2]);
  return $match[1] .'<a rel="nofollow" href="http://'. $match[2] .'" title="'. $match[2] .'">'. $caption .'</a>'. $match[3];
}


